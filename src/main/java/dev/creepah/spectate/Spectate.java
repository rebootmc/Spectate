package dev.creepah.spectate;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import dev.creepah.spectate.cmd.SpectateCommand;
import dev.creepah.spectate.manager.SpectateManager;
import lombok.Getter;
import tech.rayline.core.plugin.RedemptivePlugin;
import tech.rayline.core.plugin.UsesFormats;

@UsesFormats
public final class Spectate extends RedemptivePlugin {

    private static Spectate instance;
    public static Spectate get() {
        return instance;
    }

    @Getter private SpectateManager manager;

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;
        manager = new SpectateManager();

        registerCommand(new SpectateCommand());
    }

    @Override
    protected void onModuleDisable() throws Exception {
        manager.save();
    }

    public WorldEditPlugin getWorldEdit() {
        return WorldEditPlugin.getPlugin(WorldEditPlugin.class);
    }
}
