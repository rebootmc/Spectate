package dev.creepah.spectate.cmd;

import dev.creepah.spectate.Spectate;
import dev.creepah.spectate.cmd.sub.CreateSubCommand;
import dev.creepah.spectate.cmd.sub.DeleteSubCommand;
import dev.creepah.spectate.manager.SpectateManager;
import dev.creepah.spectate.model.SpectatorRegion;
import org.bukkit.entity.Player;
import tech.rayline.core.command.*;

@CommandPermission("spectate.command")
@CommandMeta(description = "Spectate a region")
public final class SpectateCommand extends RDCommand {

    public SpectateCommand() {
        super("spectate", new CreateSubCommand(), new DeleteSubCommand());
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        Spectate plugin = Spectate.get();
        SpectateManager manager = plugin.getManager();

        if (manager.getSpectatingRegion(player) == null) {
            if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /spectate <region>");
            SpectatorRegion region = manager.getRegion(args[0]);

            if (region == null) throw new NormalCommandException("Couldn't find a region with that name!");

            manager.spectate(player, region);
            player.sendMessage(plugin.formatAt("teleporting-in").withModifier("time", plugin.getConfig().getInt("teleport-time", 5)).get());
        } else {
            manager.stopSpectating(player);
            player.sendMessage(plugin.formatAt("stopped-spectating").get());
        }
    }
}
