package dev.creepah.spectate.cmd.sub;

import com.sk89q.worldedit.bukkit.selections.Selection;
import dev.creepah.spectate.Spectate;
import dev.creepah.spectate.manager.SpectateManager;
import org.bukkit.entity.Player;
import tech.rayline.core.command.*;

@CommandPermission("spectate.create")
public final class CreateSubCommand extends RDCommand {

    public CreateSubCommand() {
        super("create");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /spectate create <name>");

        SpectateManager manager = Spectate.get().getManager();
        Selection selection = Spectate.get().getWorldEdit().getSelection(player);
        if (selection == null) throw new NormalCommandException("You must select a world edit region!");

        if (manager.getRegion(args[0]) != null) throw new NormalCommandException("There is already a region with that name!");

        manager.addNewRegion(player, args[0]);
        player.sendMessage(Spectate.get().formatAt("region-created").withModifier("name", args[0]).get());
    }
}
