package dev.creepah.spectate.cmd.sub;

import dev.creepah.spectate.Spectate;
import dev.creepah.spectate.manager.SpectateManager;
import dev.creepah.spectate.model.SpectatorRegion;
import org.bukkit.entity.Player;
import tech.rayline.core.command.*;

@CommandPermission("spectate.delete")
public final class DeleteSubCommand extends RDCommand {

    public DeleteSubCommand() {
        super("delete");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /spectate delete <region>");
        SpectateManager manager = Spectate.get().getManager();
        SpectatorRegion region = manager.getRegion(args[0]);

        if (region == null) throw new NormalCommandException("Couldn't find a region with that name!");

        manager.deleteRegion(region);
        player.sendMessage(Spectate.get().formatAt("deleted").withModifier("region", region.getName()).get());
    }
}
