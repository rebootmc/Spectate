package dev.creepah.spectate.manager;

import com.sk89q.worldedit.bukkit.selections.Selection;
import dev.creepah.spectate.Spectate;
import dev.creepah.spectate.model.PlayerSave;
import dev.creepah.spectate.model.SpectatorRegion;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import tech.rayline.core.util.Point;
import tech.rayline.core.util.RunnableShorthand;

import java.util.*;
import java.util.stream.Collectors;

public final class SpectateManager {

    private List<SpectatorRegion> regions = new ArrayList<>();
    private Map<PlayerSave, SpectatorRegion> spectators = new HashMap<>();
    private List<UUID> waiting = new ArrayList<>();
    private FileConfiguration config = Spectate.get().getConfig();

    public SpectateManager() {
        Spectate plugin = Spectate.get();

        regions.addAll(config.getStringList("saved-regions")
                .stream().map(SpectatorRegion::new)
                .collect(Collectors.toList()));

        // Ensure that they do not leave the region they're allowed to spectate in.
        plugin.observeEvent(PlayerMoveEvent.class)
                .subscribe(event -> {
                    Player player = event.getPlayer();
                    SpectatorRegion region = getSpectatingRegion(player);

                    if (waiting.contains(player.getUniqueId())) {
                        if (event.getTo().getX() != event.getFrom().getX() && event.getTo().getZ() != event.getFrom().getZ()) {
                            waiting.remove(player.getUniqueId());
                            player.sendMessage(plugin.formatAt("teleport-cancelled").get());
                            return;
                        }
                    }

                    if (region != null) {
                        if (!region.getRegion().inRegion(Point.of(player.getLocation()))) {
                            player.teleport(region.getPoint().in(region.getWorld()));
                        }
                    }
                });

        // Try to restore the player's items and all that before they leave the game.
        plugin.observeEvent(PlayerQuitEvent.class)
                .filter(event -> getSpectatingRegion(event.getPlayer()) != null)
                .subscribe(event -> {
                   stopSpectating(event.getPlayer());
                });

        // Stop players using other commands while spectating.
        plugin.observeEvent(PlayerCommandPreprocessEvent.class)
                .filter(event -> getSpectatingRegion(event.getPlayer()) != null)
                .subscribe(event -> {
                    String msg = event.getMessage().toLowerCase();
                    if (msg.startsWith("/spectate")) return;

                    event.setCancelled(true);
                    event.getPlayer().sendMessage(plugin.formatAt("no-commands").get());
                });
    }

    public void save() {
        List<String> list = new ArrayList<>();
        regions.forEach(r -> list.add(r.serialize()));
        config.set("saved-regions", list);
        Spectate.get().saveConfig();
    }

    // Create a new spectator region.
    public void addNewRegion(Player player, String name) {
        Selection selection = Spectate.get().getWorldEdit().getSelection(player);

        SpectatorRegion region = new SpectatorRegion(
                name, player.getLocation().getWorld(),
                Point.of(player.getLocation()),
                Point.of(selection.getMaximumPoint()),
                Point.of(selection.getMinimumPoint())
        );

        regions.add(region);
    }

    // Begins a player spectating the provided region.
    public void spectate(Player player, SpectatorRegion region) {
        waiting.add(player.getUniqueId());

        RunnableShorthand.forPlugin(Spectate.get()).with(() -> {
            if (waiting.contains(player.getUniqueId())) {
                spectators.put(new PlayerSave(player), region);
                waiting.remove(player.getUniqueId());

                player.teleport(region.getPoint().in(region.getWorld()));
                RunnableShorthand.forPlugin(Spectate.get()).with(() -> player.setGameMode(GameMode.SPECTATOR)).later(2);
                player.sendMessage(Spectate.get().formatAt("now-spectating").withModifier("region", region.getName()).get());
            }
        }).later(20 * config.getInt("teleport-time", 5));
    }

    // Returns a player to their original location after spectating a region.
    public void stopSpectating(Player player) {
        PlayerSave save = getPlayerSave(player);
        if (save != null) save.reset();
        spectators.remove(save);
    }

    // Delete a spectator region.
    public void deleteRegion(SpectatorRegion region) {
        spectators.keySet().stream().filter(save -> spectators.get(save).equals(region)).forEach(save -> stopSpectating(save.getPlayer()));
        regions.remove(region);
    }

    // Returns the region a player is spectating, or null if they are spectating no region.
    public SpectatorRegion getSpectatingRegion(Player player) {
        for (PlayerSave save : spectators.keySet()) {
            if (save.getPlayer().equals(player)) return spectators.get(save);
        }

        return null;
    }

    // Get the saved player data for the provided player.
    public PlayerSave getPlayerSave(Player player) {
        for (PlayerSave save : spectators.keySet()) {
            if (save.getPlayer().equals(player)) return save;
        }

        return null;
    }

    // Attempt to find a spectator region with the provided name.
    public SpectatorRegion getRegion(String name) {
        for (SpectatorRegion region : regions) {
            if (region.getName().equalsIgnoreCase(name)) return region;
        }

        return null;
    }
}
