package dev.creepah.spectate.model;

import lombok.Data;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.Collection;

@Data
public final class PlayerSave {

    private final Player player;
    private ItemStack[] inventory;
    private ItemStack[] armour;
    private Location location;
    private Collection<PotionEffect> potionEffects;
    private float experience;
    private GameMode mode;

    public PlayerSave(Player player) {
        this.player = player;
        inventory = player.getInventory().getContents();
        armour = player.getInventory().getArmorContents();
        location = player.getLocation().clone().add(0, 1, 0);
        potionEffects = player.getActivePotionEffects();
        experience = player.getExp();
        mode = player.getGameMode();
    }

    public void reset() {
        player.teleport(location);
        player.setGameMode(mode);
        player.getInventory().setContents(inventory);
        player.getInventory().setArmorContents(armour);
        player.setExp(experience);
        potionEffects.forEach(p -> p.apply(player));
    }
}
