package dev.creepah.spectate.model;

import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.World;
import tech.rayline.core.util.Point;
import tech.rayline.core.util.Region;

@Data
public final class SpectatorRegion {

    private String name;
    private World world;
    private Point point;
    private Region region;

    public SpectatorRegion(String name, World world, Point point, Point one, Point two) {
        this.name = name;
        this.world = world;
        this.point = point;
        this.region = new Region(one, two);
    }

    public SpectatorRegion(String str) {
        String[] split = str.split("~");

        this.name = split[0];
        this.world = Bukkit.getWorld(split[1]);
        this.region = new Region(deserializePoint(split[2]), deserializePoint(split[3]));
        this.point = deserializePoint(split[4]);
    }

    public String serialize() {
        return name + "~" + world.getName() + "~" + serializePoint(region.getMax()) + "~" + serializePoint(region.getMin()) + "~" + serializePoint(point);
    }

    private String serializePoint(Point point) {
        return point.getX() + ":" + point.getY() + ":" + point.getZ();
    }

    private Point deserializePoint(String str) {
        String[] split = str.split(":");
        return new Point(Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), 90, 0);
    }
}
